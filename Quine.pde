// Processing Quine by Sejo Vega-Cebrián
String [] s = {
  "// Processing Quine by Sejo Vega-Cebrián",
  "String [] s = {",
  "};",
  "// Print first lines",
  "for(int i=0; i<2; i++){",
  "  println(s[i]);",
  "}",
  "/* Print each line of the array s indented with two spaces (32), ",
  "enclosed by quotation marks (34), and with a comma (44) at the end) */",
  "for(int i=0; i<s.length; i++){",
  "  println(str(char(32))+char(32)+char(34)+s[i]+char(34)+char(44));",
  "}",
  "// Print remaining lines",
  "for(int i=2; i<s.length; i++){",
  "  println(s[i]);",
  "}"
};
// Print first lines
for(int i=0; i<2; i++){
  println(s[i]);
}
/* Print each line of the array s indented with two spaces (32), 
enclosed by quotation marks (34), and with a comma (44) at the end) */
for(int i=0; i<s.length; i++){
  println(str(char(32))+char(32)+char(34)+s[i]+char(34)+char(44));
}
// Print remaining lines
for(int i=2; i<s.length; i++){
  println(s[i]);
}
